/*
* cookie notification popup
* https://html-online.com/articles/cookie-consent-warning-strap-website/
*/

jQuery(document).ready(function () {
  setTimeout(function ()
{
    jQuery('#consent').fadeIn(200);
  }, 4000);

  jQuery('#closeConsent, .consentOK').click(function () {
      jQuery('#consent').fadeOut(200);
    });
});
