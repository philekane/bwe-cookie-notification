=== Cookie Notification/Privacy Policy Update Popup ===
Contributors: Phil Kane
Tags: cookie notification, gdpr
Requires at least: 4.9.5
Tested up to: 4.9.8
Requires PHP: 5.6 >

Notify visitor of cookie settings unless do not track is enabled
also notifies visitor if privacy-policy has been updated since their last visit

== Description ==
Notify visitor of cookie settings unless do not track is enabled
also notifies visitor if privacy-policy has been updated since their last visit

== Installation ==
Install

== Frequently Asked Questions ==
FAQ

== Screenshots ==
1. Cookie Notification Popup
2. Privacy Policy Update Popup
3. links to reviews
