biz-sites-etc-cookie-notification - A WordPress Plugin

A popup based on https://html-online.com/articles/cookie-consent-warning-strap-website/

Lets visitors know that cookies are being set on their browser.

It also looks to see if the visitor has visited before and has a cookie, if it is older than the privacy-policy a popup will tell them the privacy-policy has changed. 

The code is looking for page with the title Privacy Policy, if yours is named differently you will have to change this or do a query with the id.

It looks to see if Do Not Track (DNT) is enabled and if so it does not set the cookie or popup the warning strap that
there are cookies being set.

However if you want to honor the DNT completely; no cookies from whole site
add the following code to your functions.php in your theme

function biz_sites_etc_do_not_track() {

// check if do not track is enabled if so remove cookies

$doNotTrack = $_SERVER['HTTP_DNT'];

if( $doNotTrack == 1 ){
if (isset($_SERVER['HTTP_COOKIE'])) {
$cookies = explode(';', $_SERVER['HTTP_COOKIE']);
foreach($cookies as $cookie) {
$parts = explode('=', $cookie);
$name = trim($parts[0]);
setcookie($name, '', time()-1000);
setcookie($name, '', time()-1000, '/');
}
}
return;
}
}
add_action('init', 'biz_sites_etc_do_not_track', 999);
 
Install:

Download zip file and upload to WordPress installation

