<?php
/**
 * A Cookie_Notification class that pops up notification of cookies
 *  and of privacy policy change
 *
 * PHP version 7.2
 *
 * @category Cookie_Notification
 * @package  Category
 * @author   Phil Kane <pkane-pluginDev@spindry.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://business-websites-etc.com *
 */
if (!class_exists('Biz_Sites_Etc_Cookie_Notification_Settings')) {
    /**
     * A Cookie_Notification class that pops up notification of cookies
     *  and of privacy policy change
     *
     * @category Cookie_Notification
     * @package  Category
     * @author   Phil Kane <pkane-pluginDev@spindry.com>
     * @license  MIT https://opensource.org/licenses/MIT
     * @link     https://business-websites-etc.com *
     */
    class Biz_Sites_Etc_Cookie_Notification_Settings
    {
        /**
         * Construct the plugin object
         */
        public function __construct()
        {
            // register actions
            add_action('admin_init', array(&$this, 'admin_init'));
            //    add_action('admin_menu', array(&$this, 'add_menu'));
        } // END public function __construct

        /**
         * Hook into WP's admin_init action hook
         *
         * @return null
         */
        public function admin_init()
        {
            // register your plugin's settings

            // Possibly do additional admin_init tasks
        } // END public static function activate

    } // END class Biz_Sites_Etc_Cookie_Notification_Settings
} // END if(!class_exists('Biz_Sites_Etc_Cookie_Notification_Settings'))