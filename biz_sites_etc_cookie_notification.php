<?php
/**
 * Basic WordPress Plugin Cookie Notification notifies visitor that cookies
 *  are set unless "Do not track" is enabled. If privacy-policy page has been
 *  updated since the visitors last visit a popup will notify visitor of this
 *  and recommend reading the update.
 *
 * PHP version 7.2
 *
 * @category Cookie_Notification
 * @package  Biz_Sites_Etc_Cookie_Notification
 * @author   Phil Kane <pkane-pluginDev@spindry.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @version  1.0
 * @link     https://business-websites-etc.com *
 */
/*
Plugin Name:  Biz Sites Etc Cookie Notification
Plugin URI:   https://plugins.business-websites-etc.com/cookie-notification
Description:  Basic WordPress Plugin Cookie Notification notifies visitor that cookies are set unless "Do not track" is enabled. If privacy-policy page has been updated since the visitors last visit a popup will notify visitor of this and recommend reading the update.
Version:      1.0
Author:       Phil Kane, Business Websites, Et cetera
Author URI:   https://business-websites-etc.com/about
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  biz-sites-etc-cookie-notification
Domain Path:  /languages

biz_sites_etc_cookie_notification is free software: you can redistribute
it and/or modifyit under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 2 of the License,
or any later version.

biz_sites_etc_cookie_notification is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with biz_sites_etc_cookie_notification. If not, see {URI to Plugin License}.

if you want to honor "Do Not Track" completely no cookies from whole site
add the following code to your functions.php in your theme

function biz_sites_etc_do_not_track() {

// check if do not track is enabled if so remove cookies

$doNotTrack = $_SERVER['HTTP_DNT'];

if( $doNotTrack == 1 ){
if (isset($_SERVER['HTTP_COOKIE'])) {
$cookies = explode(';', $_SERVER['HTTP_COOKIE']);
foreach($cookies as $cookie) {
$parts = explode('=', $cookie);
$name = trim($parts[0]);
setcookie($name, '', time()-1000);
setcookie($name, '', time()-1000, '/');
}
}
return;
}
}
add_action('init', 'biz_sites_etc_do_not_track', 999);
 */

if (!defined('ABSPATH')) {
    die('No direct access allowed');
}

if (!class_exists('Biz_Sites_Etc_Cookie_Notification')) {
    /**
     * A Cookie_Notification class that pops up notification of cookies
     *  and of privacy policy change
     *
     * @category Cookie_Notification
     * @package  Category
     * @author   Phil Kane <pkane-pluginDev@spindry.com>
     * @license  MIT https://opensource.org/licenses/MIT
     * @link     https://business-websites-etc.com *
     */
    class Biz_Sites_Etc_Cookie_Notification
    {
        /**
         * Construct the plugin object
         */
        public function __construct()
        {
            // Initialize Settings
            include_once sprintf("%s/settings.php", dirname(__FILE__));
            $Biz_Sites_Etc_Cookie_Notification_Settings = new Biz_Sites_Etc_Cookie_Notification_Settings();

            // Register custom post types
            include_once sprintf("%s/cookies/cookies-notification.php", dirname(__FILE__));
            $Biz_Sites_Etc_Cookie_Notification = new Cookie_Notification();

            //$plugin = plugin_basename(__FILE__);
            //add_filter("plugin_action_links_$plugin", array(&$this, 'plugin_settings_link'));
        } // END public function __construct
        /**
         * Activate the plugin
         *
         * @return null
         */
        public static function activate()
        {
            // Do nothing
        } // END public static function activate
        /**
         * Deactivate the plugin
         *
         * @return null         *
         */
        public static function deactivate()
        {
            // Do nothing
        } // END public static function deactivate
    } // END classBiz_Sites_Etc_Cookie_Notification
} // END if(!class_exists('Biz_Sites_Etc_Cookie_Notification'))

if (class_exists('Biz_Sites_Etc_Cookie_Notification')) {
    // Installation and uninstallation hooks
    register_activation_hook(__FILE__, array('Biz_Sites_Etc_Cookie_Notification', 'activate'));
    register_deactivation_hook(__FILE__, array('Biz_Sites_Etc_Cookie_Notification', 'deactivate'));
    // instantiate the plugin class
    $Biz_Sites_Etc_Cookie_Notification = new Biz_Sites_Etc_Cookie_Notification();
}
