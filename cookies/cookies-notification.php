<?php
/**
 * A Cookie_Notification class that pops up notification of cookies
 *  and of privacy policy change
 *  
 * PHP version 7.2
 *
 * @category Cookie_Notification
 * @package  Category
 * @author   Phil Kane <pkane-pluginDev@spindry.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://business-websites-etc.com * 
 */
if (!class_exists('Cookie_Notification')) {

    class Cookie_Notification
    {
        /**
         * The Constructor
         */
        public function __construct()
        {
            // register actions
            add_action('init', array(&$this, 'init'));
            //add_action('admin_init', array(&$this, 'admin_init'));

        } // END public function __construct()

        /**
         * Hook into WP's init action hook
         *
         * @return null
         */
        public function init()
        {
            // Initialize cookie notification
            add_action('init', array(&$this, 'biz_sites_etc_cookies_notification'), 999);
        } // END public function init()

        /**
         * Set cookie notification notice
         *
         * @return null
         */
        public function biz_sites_etc_cookies_notification()
        {
            /*
             * check if do not track is enabled if so remove cookies
             */
            
             if (isset($_SERVER['HTTP_DNT']) && $_SERVER['HTTP_DNT'] === 1) {
                if (isset($_SERVER['HTTP_COOKIE'])) {
                    $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
                    foreach ($cookies as $cookie) {
                        $parts = explode('=', $cookie);
                        $name = trim($parts[0]);
                        setcookie($name, '', time() - 1000);
                        setcookie($name, '', time() - 1000, '/');
                    }
                }
                return;
            }

            /**
             * Check if there is a privacy policy if so get the post id
             * and set a popup cookie notification and set cookie if notreturn
             */
            $page = get_page_by_title('Privacy Policy');
            if (!$page) {
                unset($_COOKIE['wp_cookie_notification']);
                return;
            }
            //get post id
            $postId = $page->ID;
            // Time of user's visit
            $visit_time = date('F j, Y g:i a');
            

            // Check if cookie is already set
            if (isset($_COOKIE['wp_cookie_notification'])) {

                // Do this if cookie is set

                // Use information stored in the cookie
                $lastvisit = $_COOKIE['wp_cookie_notification'];
              

                // Delete the old cookie so that we can set it again with updated date and time
                unset($_COOKIE['wp_cookie_notification']);

                // Set the cookie with the new date
                //setcookie('wp_cookie_notification', $visit_time, time() + 31556926);
                setcookie('wp_cookie_notification', $visit_time);

                /*
                 *  Check if privacy policy has changed since $lastvisit
                 *  get the privacy policy page date from database post = 3
                 */
                $privacyPolicyDate = get_post_modified_time('F d, Y g:i a', '', $postId);

                if (strtotime($privacyPolicyDate) > strtotime($lastvisit)) {

                    /*
                     * add jquery script and css that pops up cookie notification BAR
                     */
                    add_filter('the_content', 'get_privacy_popup');
                    add_action('wp_enqueue_scripts',  'cookie_notification_script');
                }

            } else {

                // Do this if the cookie doesn't exist

                /*
                 * add jquery script and css that pops up cookie notification BAR
                 */
                add_filter('the_content', 'get_notification_popup');
                add_action('wp_enqueue_scripts', 'cookie_notification_script');

                // Set the cookie
                setcookie('wp_cookie_notification', $visit_time, time() + 31556926);

            }
            /**
             *  Popup cookie notification BAR
             *
             * @param mixed[] $content is the page or post with the_content
             *
             * @return string the_content with html popup appended
             */
            function get_notification_popup($content)
            {
                return $content . '<div id="consent">
					<div id="closeConsent">X</div>' .
                __("This website is using cookies.", "biz_sites_etc_cookie_notification") . '<a href="' . esc_url('/privacy-policy') . '"    target="_blank">' . __(" More info", "biz_sites_etc_cookie_notification") . '</a> <a class="consentOK">' . __("That's Fine", "biz_sites_etc_cookie_notification") . '</a>
			</div>';
            }
            /*
             *  popup privacy policy notification BAR
             *
             * @param mixed[] $content is the page or post with the_content
             *
             * @return string the_content with html popup appended
             */
            function get_privacy_popup($content)
            {

                return $content . '<div id="consent">
					<div id="closeConsent">X</div>' .
                __("Our privacy policy has been updated. Please read the Revisions in the privacy policy.", "biz_sites_etc_cookie_notification") . '<a href="' . esc_url('/privacy-policy') . '"' . 'target="_blank">' . __(" Privacy Policy", "biz_sites_etc_cookie_notification") . '</a>. <a class="consentOK">OK</a>
			</div>';
            }

            /**
             *  JQuery script and css that pops up cookie notification BAR and privacy policy BAR
             *
             * @return null
             */
            function cookie_notification_script()
            {
                wp_enqueue_script('jquery');
                wp_enqueue_script('cookie-notification-script', plugin_dir_url(__DIR__) . 'js/cookie-notification-script.js', array('jquery'), '1.0');
                wp_enqueue_style('cookie-notification.min', plugin_dir_url(__DIR__) . 'css/cookie-notification.min.css', array(), null, 'all');
            }

        }
    }
   
}
